import unittest

from sgp4.earth_gravity import wgs72
from sgp4.io import twoline2rv
from satnogs_api_client import fetch_satellites, DB_BASE_URL

from satellite_tle import fetch_tles

# FIXME: At the moment (2019-06-23) the CalPoly webserver distributing TLEs is down,
# so we don't have a source for the following satellites.
# The tests shouldn't rely on the webserver, but e.g. mock the results instead.
known_missing_norad_ids = set([38760, 40968, 37224, 39469, 43699])


class TestFetchTlesMethods(unittest.TestCase):
    def test_fetch_tles_iss(self):
        norad_ids = [25544]  # ISS
        tles = fetch_tles(norad_ids)

        # Assert that a TLE for ISS was returned
        self.assertEqual(list(tles.keys()), [25544])

        # Asser that this TLE is in fact for the ISS
        sat = twoline2rv(tles[25544][1][1], tles[25544][1][2], wgs72)
        self.assertEqual(sat.satnum, 25544)

    def test_fetch_tles_against_satnogs_db(self):
        '''
        Check if TLEs for all satellites in the satnogs-db are found.

        NOTE: This is allowed to fail if there are satellites in the satnogs-db for which
              none of our known sources provide TLEs!
        '''

        # Fetch the satellites of interest from satnogs-db
        sats = fetch_satellites(url=DB_BASE_URL, max_entries=None)
        satnogs_db_norad_ids = set(sat['norad_cat_id'] for sat in sats
                                   if sat['status'] != 're-entered')

        # Remove satellites with temporary norad ids
        temporary_norad_ids = set(filter(lambda norad_id: norad_id >= 99900, satnogs_db_norad_ids))
        satnogs_db_norad_ids = satnogs_db_norad_ids - temporary_norad_ids
        satnogs_db_norad_ids = satnogs_db_norad_ids - known_missing_norad_ids

        # Fetch TLEs for the satellites of interest
        tles = fetch_tles(satnogs_db_norad_ids)

        missing_norad_ids = set(satnogs_db_norad_ids) - set(tles.keys())
        sats_missing = list(sat for sat in sats if sat['norad_cat_id'] in missing_norad_ids)
        sats_missing.sort(key=lambda sat: int(sat['norad_cat_id']))
        error_message = 'Satellites in SatNOGS-DB but without a TLE source:\n'
        for sat in sats_missing:
            error_message += '{} ({})\n'.format(sat['norad_cat_id'], sat['name'])

        self.longMessage = False
        self.assertEqual(set(satnogs_db_norad_ids),
                         set(tles.keys()),
                         msg=error_message)
